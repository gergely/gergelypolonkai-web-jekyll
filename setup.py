from setuptools import setup, find_packages


setup(name='gergelypolonkaieu-site',
      version='1.0.0',
      description='gergely.polonkai.eu site',
      author='Gergely Polonkai',
      author_email='gergely@polonkai.eu',
      license='All Rights Reserved',
      packages=find_packages('.'),
      zip_safe=False)
