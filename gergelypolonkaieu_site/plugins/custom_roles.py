from datetime import datetime

from docutils import nodes
from docutils.parsers import rst


def keyboard_role(name, rawtext, text, lineno, inliner, options=None, content=None):
    return [nodes.raw('', f'<kbd>{text}</kbd>', format='html')], []


def superscript_role(name, rawtext, text, lineno, inliner, options=None, content=None):
    return [nodes.raw('', f'<sup>{text}</sup>', format='html')], []


def del_role(name, rawtext, text, lineno, inliner, options=None, content=None):
    return [nodes.raw('', f'<del>{text}</del>', format='html')], []


def exp_years_role(*args, **kwargs):
    now = datetime.utcnow()
    exp_range = now - datetime(1993, 9, 1)
    exp_years = int(exp_range.total_seconds() / 31536000)

    html_text = f'<span class="definition" title="As of {now.year}">{exp_years}</span>'

    return [nodes.raw('', html_text, format='html')], []


def register_roles():
    rst.roles.register_local_role('kbd', keyboard_role)
    rst.roles.register_local_role('sup', superscript_role)
    rst.roles.register_local_role('del', del_role)
    rst.roles.register_local_role('exp_years', exp_years_role)


def register():
    register_roles()
