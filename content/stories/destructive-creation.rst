Destructive Creation
####################

:date: 2011-12-30T18:22Z
:category: stories
:status: published

They stood on the top of a hill, looking sadly at the town and the forest slowly devoured by it.
They really tried everything.  They asked the workers, even the higher management to stop this, or
it won’t end well.  They told that the forest will get fed up with this and will do whatever it
must to protect itself.  No one listened.  No one ever listened to what they said, but still, they
were always right.

Today’s try was the last one to stop the logging.  Naturally, they laughed in their faces and
shooed them away.  They always got away with that much, no one ever hunted them as they were only
two “green guys” who only have big mouths.  But the spirit of the forest didn’t think that.  It
saw the potency and power in them to make their prophecies happen.  But somehow it wasn’t enough
to teach the lesson to a whole city, as the forest spirit was weak, too.  It was weakened by the
death of its children, so as the two youngster left the town, they made an alliance without words.

Everything looked small and insignificant from the top of the hill, but still, they felt it
actually isn’t.  If they do it here and now, thousands, maybe tens of thousands will die.  Once.
The others may be saved, if they want to be saved.  Or they will be marked as murderers and will
be chased forever.  But they will figure that out; this is much more important now.

They sighed together.  “Please, stop it.  It’s still not too late.”  It just got through their
mind, and in the very same moment a cold chill ran down from the hill, through the town.  But the
machines went on, only a few workers noticed the strange wind.  They may have understood, some of
them even went off for a short rest.  They may survive, if the forest wants that.

The boy raised his right arm, his palms towards the sky as if he’s asking for aid.  The girl took
his other hand, then offered her body to the spirit of the forest.  A tear drop ran down on their
faces, accompanied by a smile.  They felt calm, as if they knew only better may come after this.
But they still waited, maybe they understand everything down there.  But finally, that hand in the
air dropped, as if life escaped from it suddenly.  Quiet rumble crawled up the hill: the spirit of
the forest started off with all the powers of them for its destructive journey.  They turned
around, satisfied, and left for somewhere, hopefully for home.

They still heard the noise from down there, but they didn’t have to look to know what is
happening.  The forest got fed up and stroke back.  Huge roots pulled the machines under the
ground and smashed houses to dust.  They destroyed, devoured everything, not sparing anyone living
there.  Just like they did with its children.  The once peaceful forest was raging, engulfing
everything within reach.

The murmuring slowly faded away, and at the place of the town there was only a cloud of dust at
the foot of the hill.  And all of a sudden, with the promise of new life, it started raining.
