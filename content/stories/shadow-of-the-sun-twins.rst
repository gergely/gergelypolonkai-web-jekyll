Shadow of the Sun Twins
#######################

:date: 2018-07-29T14:54Z
:category: stories
:tags: scifi
:url: stories/shadow-of-the-sun-twins
:save_as: stories/shadow-of-the-sun-twins/index.html
:status: published
:author: Gergely Polonkai

The below sci-fi short is a reply for `this writing prompt
<https://mastodon.social/@WritingPrompts/100457172434884207>`_:

    Life is hard for a dirty salvager on this junk planet. If the heat of the desert's twin suns
    don't kill you, the molten rain coming down from the ship graveyard orbiting the planet
    might. But today is a good day. Today you found what looks to be a fully operational
    spacecraft.

The desert was calm.  Hot as ever, but at least the acidic molten rain stopped almost a week ago
now.  The couple darted from wreck to wreck, just like every day since they crashed on this living
hell almost two decades ago.  Salvagers.  Thatʼs what they were for their whole lives, which was
almost forty years now.  Everyone despised their kind yet, a lot of of megacorps were relying on
them.  They could easily identify parts their commissioners wanted, sometimes hundreds of feet
away.  They werenʼt the best of the best, but they were good.

Their clothes are mostly rags, but they didnʼt stand out during the rare occasions when they
visited one of the local colonies.  Everyone and everything was dirty and wrecked here.  It was a
home for outcasts and criminals.

Shiny ships landed about once a month to pick up things they ordered, and trade it for food and
water.  They had some old tools to sell, but you had to be either wealthy or revered to buy them.

Thatʼs how Raleh and Gerth got their only wealth, the binoculars.  The image they provided was
crystal clear, and the software running on it could easily identify any types of spacecraft they
were specialised in.  It costed them a small fortune, in both sense of the word.  They still made
jokes occasionally about that day when they planned their uncertain future years after getting
them.

“Enter” a machine voice said, after the door on the spacecraft, that seemed to be a wreck, opened.

They hesitated.  Neither of them saw a ship on this planet before that was capable of
communication not counting the crafts of their employers.  The ship was small, more like a
personal carrier than a battle cruiser.  It didnʼt have any sign of authority or ownership except
the confederation flag painted on both sides.

“Enter” the voice repeated.

Raleh was always more brave, recklessly so.  After stepping inside, she waved for Gerth.

“Come inside!  We could use this to make ourself a future.” she said, but Gerth was still
shuffling outside.

“What, you think anyone would buy a ship that allows anyone on board?” Gerth replied.  And indeed,
federal law stated anyone approaching a spacecraft without proper authorisation would be zapped to
death by the shipʼs defense system.  They saw it countless times.  They even buried a friend who
tried it.

“Who cares?  If nothing else, we can rip it to pieces and sell all the parts.” Raleh insisted,
then disappeared inside the craft.

Inside it was dark, or at least it seemed so after getting out of the scorching light of the sun
twins.  Her eye got used to it eventually.  The ship seemed completely intact.  No burning, no
sign of crashing in the inside.

“Hon!”  It was Gerth, and sounded really excited.  “Honey, come out, I have to show you
something!”

Raleh left through the small door to find her partner squatting next to the ship.  She was
touching the shield, examining it closely.

“This ship didnʼt crash land here.”  she announced.  “It was shot several times, and one of the
engines were hit, but it simply landed.”

“Go figure,” Raleh replied, “the inside is good as new.”

The two entered the ship again and looked around.

“Computer!”  Gerth said in loud and firm tone.  “Self diagnosis.”  She knew well from the times
they were salvaging in space that a confederation ship will identify itself as part of the
response.

“Cruiser type PC-79-1, registration code MID-683, owned by the Intelligence Department of the
Confederate Military.”  the machine told, and continued.  “Shield operational and lowered down.
Hull 97% intact.  Solar panels providing 230% of required energy.  All reserve batteries at 100%.
Engine one operational.  Engine two operational.  Engine three damaged, manual intervention
needed.  Ship is capable of lift off and flight.  Software level diagnosis must be requested
explicitly.”

“Thatʼs nice” Gerth summed.  “But why would the Intelligence department leave a functioning ship
in the middle of hell?”

“Beats me.  But we should decide what to do with it before someone else finds it” Raleh said, and
now it was her time to get nervous.

“Computer, close doors” Gerth commanded and waited until the ship carries out her order.  “Just so
no one sees us from the distance.  Computer!  Identify your captain!”

On the screen, a picture of a woman appeared.  She was probably around her forties with short
blonde hair and deep green eyes, and a fierce, but happy look on her face.  Her uniform showed the
rank of a confederate colonel.  If it werenʼt for the rags and the messy hair, it could have
easily been Gerth.

“Colonel Gerthrud Miartan” the ship announced.  “Welcome back on board.”
