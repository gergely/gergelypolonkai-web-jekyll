White cell
##########

:date: 2009-07-28T20:29Z
:category: stories
:status: published

Thomas stepped in his room.  He felt strange but he couldn’t explain it until he closed the door
behind him.  The furniture, all his personal stuff, even the window and the door has disappeared,
and the walls were glowing bright white.  For a brief moment he thought he got blind, but then
realised he could see his own body just fine.  He tried to open the door where it used to be, but
he grabbed wall instead of the knob.  Suddenly he heard a calm voice from his back.

“Hello, Thomas.  I was waiting for you.”

“Who are you?  And what happened to my room?  Or my senses?”

“Nothing.  Both you and your room are the same as you left about two hours ago.”

The man was tall and thin, his long white hair reached the middle of his back.  He looked young,
but his deep green eyes radiated a long life and wisdom.

“I am a magician.  Just like how you like to call yourself” he added.

“Because I am!  Now let me out of here!”

“Please, sit down” the man asked, but Thomas refused.

“Why would I?”

“Because I asked you nicely.”

“But I don’t want to sit down.  I want to get back to my room and to my life!  My disciples are
waiting for me!”

“You can leave freely, whenever you please.”

“How could I?  Even the door has disappeared.”

The white haired man disappeared without an answer.  Thomas became more furious every minute,
which transitioned to despair.  He hit the wall until his fist was bleeding, although the
bloodstains couldn’t be seen on the glowing surface.

“Where are you, *magician*?” he asked.  “You locked me up here but you fear to face me?  Come back
so I can beat you up until you release me!”

“I was always here, Thomas.”  The magician stood exactly whence he disappeared. “Why are you so
upset?  You are safe and sound in your room.  The bed, your wardrobe, the door and window are all
at their usual places.  You simply have to step to the door and leave.”

“What are you talking about?  You locked me up in this white cell, which has no windows, no door,
just the glowing white walls.  There’s nothing here, especially no exit.  Now bring me back to my
room!”

Thomas tried to hit the magician, but he was too slow for his supernatural senses and dexterity.

“I don’t want to fight you” the sorcerer said.  “I just want you to understand the world around
you so you don’t have to lie to your disciples any more.”

Thomas tried even more, but he was no real opponent for the sorcerer.

“Would you feel better if you could hit me?”

“I would feel better if you would finally release me!”

“Come on, Thomas, you would better…” the man began, but Thomas interrupted.

“Don’t call me Thomas!  I hate that name.  I don’t use it for years.”

“Your parents gave you that name.  You shouldn’t make them sad by dropping the name they gave
you.  Yes, it is common and simple.  But it’s not the name that will make you different from
others.”

“What do you know about names?  You didn’t even tell me yours”

“Because I don’t have one.  My parents didn’t want me, so they didn’t give me a name.” the man
replied, with a still calm voice.  “Call me Anonymus, or whatever you like.”

While he was talking, Thomas continued the attacks, but without success.

“Your hand is bleeding.  It should be treated.”

“I would, if you would let me back to my room!”

“You still don’t understand.  You are worse than I hoped.  Now please, sit down so I can show you
the way back to your beloved room.”

Thomas attacked again with a loud battle cry.  The wizard stepped to the left with ease, grabbed
Thomas’ arm and forced him to a sitting position with a quick move.

“I asked you twice to calm down and sit.  If you wouldn’t be such an arrogant fool, you would sit
in your room by now.”  His voice was thundering and shocking which scared Thomas, so he remained
sitting.

“Thank you” the wizard continued.  “Only one thing remains.  You must believe that I’m not lying
when I say you are in your room.”

Thomas didn’t say a word.  He thought this guy is insane, or even a pervert that he keeps him
here.

“Okay, so you don’t believe me.  In this case let’s just… *assume* you are in your room.  Your
window used to be here, right behind me.  You keep telling your disciples that visualisation is
so important.  Now do it!”

Thomas tried.  He believed that this stranger will keep him here forever, so he had nothing to
loose.  Then, when he was focusing for ten minutes, the silhouettes of the window appeared on the
wall, right behind the sorcerer.  He was surprised as although he really did say to his disciples,
he never really succeeded in visualisation.  After the surprise caused by the window, Thomas
looked around and could see everything crystal clear in his room.

“Just as I said” said the wizard.  “You could have left whenever you wanted.”

“What kind of bad joke is this?” Thomas asked, still shocked.

“It wasn’t a joke.  It was your imagination.  Or… was it mine?”
