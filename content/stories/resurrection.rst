Resurrection
############

:date: 2009-10-31T12:16Z
:category: stories
:status: published

“Good morning, inspector.”

“Good morning.  What happened?”

“We don’t know yet.  A jogger found this young man about half an hour ago.  No sign of violence,
no injuries, no nothing.  He has no ID, no driver’s license, no passport nor anything that could
give us a hint on who he might be.”

“Doctor?”

“Just as he says.  I can tell you more in the morgue.  The only thing I’m sure about is he’s dead
for about two or three hours.”

“Thank you.  Please check the surroundings, maybe we get to know something about him or his
death.”

Two strong men put the body in the van and transported it to the morgue.  They put it in the
“waiting room” until the pathologist arrives.

John woke up.

“It’s so cold here… wait a minute, where am I?” he thought and looked around.  Beds everywhere,
like the ones in a hospital, but all of them are empty.  Dim, blue light, misty, cold air.  He
tried to remember what happened to him.  His last memory was that he’s drinking wine with his
friends.  Not much, at least not that much it could hit him this hard.  He realized where he is.
“My goodness, it’s a morgue!  How the heck did I get here?”

He heard the lock clicking, and a doctor-like figure entered.  “Finally someone!” he shouted.
“Why am I in a morgue?  Is this a joke?  Because if so, it’s not funny…” he said.  The doctor, as
if he hadn’t heard John’s words, stepped next to John’s bed and lifted the plastic sheet.  His
face turned sad as he saw how young the boy was who lied in front of him.  John looked down and
was shocked.  He saw himself lying there.

“He can’t hear you.” a voice said, but John didn’t look at the source.

“Is that… me?  Am I dead?” he finally asked, still staring at his own corpse.

“Not yet, no.  But you have only a few minutes before your body gives up.”

“How did I die?” he asked, finally looking up at the man speaking.  “I can’t remember a thing.”

“Well, technically… you can say I killed you.”

John’s face changed from shocked to angry.  “What did you do?”

“Yes, you heard it right.  It was me, although it is not actually a murder.  I just asked you to
leave your body for a few minutes.  Looks like this state takes a little longer, I’m terribly
sorry about that.” the man said.  John was still shaken from the scene so it took time to
understand what he said.

“What do you mean I have only a few minutes left?” he finally asked as he calmed down.

“I trusted you, John.  I thought you are a Witch as you always said you are.  Now it seems you
don’t even know the basics.” The man was angry, John clearly felt that.  “You didn’t notice me
talking to you through dreams, and also don’t remember my command for your bodies to split.  And
last, but not least, you don’t even have an idea on how to glue them together so your life won’t
end.”

“But… this can’t happen!  This is impossible!  If anyone would have such power in their hands they
would already rule the whole universe by now!” came the reply from John, who seemed to ignore the
fact that he’s dying in front of his own eyes.

“That, my friend, is the biggest mistake of your life.  And, seeing your body here, it looks like
it is the last one.  We will meet in your next life.  It was a pleasure knowing you.”

At this very moment the doctor made the first dissection on John’s body; fresh, red blood oozed
from the wound.  John woke up instantly with a loud scream.
