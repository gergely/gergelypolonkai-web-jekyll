The Encounter
#############

:date: 2012-01-02T10:15Z
:category: stories
:status: published

Mendari stood on the top of a worn-out, old panel house.  He looked down on the masses on the
street.  “Your life depends on the fact that I have not chosen to end it yet.  As soon as my
brother is gone, no one will be left to save you” he thought, then looked to the farther parts of
the city.  In a less crowded district, he’s waiting for him in a park.  “Where else?  The concrete
jungle is not his world.  He’s weak.  He was always too weak to change.  The city grew around him
and he left in his weary park where nothing changes.”  He turned his head towards the park.  He
always sits there, and Mendari was sure it’s the same even now.  He took two quick steps and took
off from the edge of the roof; spreading his red, bat-like wings he was nose-diving towards the
ground.

Eldreth was sitting in his well known park on the most hidden bench.  No one went that way, not
even the homeless.  No one ever encountered him but still, everyone recognised this as his own
place.  But everything will change soon.  Mendari will come here and will try to destruct
everything he still got since the city was built.  He will leave soon.  Humans destroy all the
forests on this planet, and he will have less and less place to live.  Whatever is left is not
suitable for him.  He tried to move into some densely populated districts, but his head went
buzzing.  And the downtown parts are even unbearable.

He looked up for a brief moment, but even this was enough for him to see his brother, nose-diving
from the roof.  He had plenty of time.  Slowly, but with with nobility he stood up and waited
until his brother arrives.  As soon as he was close by he spread his white wings to blind his
future opponent.

Mendari came to a halt seeing those wings.  He knew exactly Eldreths powers, but he hoped he got
lazy and cannot use them.

“It seems I have underestimated you, brother” he said with irony in his voice.  “You seem to use
your powers well, especially knowing that the mayors have taken almost everything from you.”

“Fortunately, I have my reserves” Eldreth said, smiling.

They stood there for minutes, probably for hours, in front of each other.  Who passed by may have
only seen two men there.  They have dissambled their wings in this world as they knew if anyone
would have seen them they would have gotten huge attention in the noisy media of the world.  But
what they were planning right now cannot go unnoticed.  They were sure it will cause a huge media
storm at the price of their own demise.  But it doesn’t matter now.  The time has come, it must
happen here and now.

The encounter was unbearable for everyone around them.  Nothing could be seen, they just
experienced the hurricane like wind.  When they grabbed each other even the earth began to shake.
At first only around them, but they slowly spreaded to the whole city.  The walls of the panel
houses, which were not built to stand a bigger earthquake, began to crack.  The pictures on the
walls, the flowers celebrating the new mayor, and even the lamp posts dropped to the ground one by
one.  Even this was enough for the locals to panic.  They flocked to the streets and it didn’t
take much time before a group or two got to the park.  Whoever came this far could see the two
brothers in the epicentre of the quakes, with their huge wings spread, holding each other without
moving.  The trees in the park fell, and the small pond in the middle of the park, with all the
boats and the people in them quickly leaked into a newly opened gap.  Soon, some of the refugees
recognised the potential in the events and started to take pictures.

“See?” thundered Mendari.  “Nothing is enough for them!  Only success and money moves them.  Look!
LOOK!  They have surrendered their homes that were crumbled to dust but they don’t lament anything
just take pictures.  Here are all the fallen trees, some of them has people under them and what
they do?  Are they mourning?  NO!  They take pictures hoping a magazine will pay loads of money
for this trash!”

Eldreth was used to such scenes, and although his brother was trying to plant disdain and anger in
his heart, he could not pull Light to the Darkness’ side.  So Mendari, seeing his brother’s
persistence, continued.

“Do you really want to save these?  Why?  They have no use on this planet.  They never had!”

“I think you misunderstood something.  I never wanted to save *them*.  Just life.  That’s what you
cannot take from them without punishment.  For that both me and all Spirits of life will turn
against you.”

Mendari didn’t say a word, just pushed two photographers to the chasm with his wing.  They tried
to grab anything, screaming, but without success.  The ground swallowed them with a quiet rumble.

“So where are these spirits now?  Nowhere.  Why haven’t they saved this two sacred lives?  Where
are they that they couldn’t catch any of them?”  Mendari tried to give emphasis to his words by
shaking his brothers shoulders while he was taking care not to let him go.

“No one has to catch them on the way down to save them.  At the bottom there is the Spirit of the
lake.  He will catch them, and although it will hurt much, they will survive.

The demon got furious.  “How can you remain this calm?  Your life, the humans’ life is in my hands
and you still don’t fight me!”

“I’m not calm.  I know what will happen and I know it must happen this way.”

The earthquake got stronger and finally, the ground started to open below them.  At first only a
small crack flashed between them, but it became wider with time until the brothers had to release
each other.  They jumped to the air.  The people around them stopped, forgetting about the panic,
staring at them in awe.

Up until now only their minds were fighting, but now even their bodies clashed.  It was true
wrestling in the air, with the aim of bringing the other to the ground.  But Eldreth only parried,
never attacking back.  He easily evaded his brother’s moves so Mendari was trying more furiously.
They got higher in the air, but it was the angel who wanted this.  His brother followed in
delirium just to finish him already.

He doesn’t have to wait for too long.  Eldreth had to miss only a tiny move.  Mendari took
advantage and held down all of his brothers limbs.  They were plummeting towards the ground, the
demon controlling it so he would have been on top when they reach the ground.  There was
maleficence and satisfaction in his eyes looking at Eldreth pinned down body.

The angel looked up and smiled.  Endless gratitude whirled from him, even towards Mendari.

“I always loved you, my dear brother” he whispered.  “Maybe once, much later, we will see each
other again.”

They slammed to the ground with a deafening crash, almost at the middle of the place where once
the park was.  The huge dust cloud caused by the earthquakes, hid their bodies.  The noise
stopped, so as the quakes.  It was over.  They fell so hard it was fatal for both of them.

Mendari’s body exploded to millions of red shards that slowly spread in the park.  They flooded
the region as a dreadful dollop, biting the flesh from everyone’s bones.

Eldreth turned into small, fog-like drops of water.  These blended together with the red mass,
diluting it.  He was trying to protect life even in his death.

The red fog slowly covered the city.  Weakened by Eldreth’s powers it couldn’t physically harm
people, but it turned to be more dangerous this way.  It burned people’s feelings and emotions.

With the earthquake stopping the wind began to blow again.  A desert-like storm approached the
city, which was direful together with shards of the red fog.  It engulfed everyone and everything.
Mendari wanted to spare no one.
