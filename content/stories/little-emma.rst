Little Emma
###########

:date: 2012-08-07T14:18Z
:category: stories
:status: published

As of date, little Emma hasn’t eaten for weeks.  The whole family feared for her as only those
tubes entangling her body kept her alive.  Still, her eyes seemed happy, though she was dizzy and
sometimes saw little globes of light.  She was awake for hours, much longer than she used to be in
the last few weeks.  Her parents sat next to her for minutes, caressing her, just to leave the
room moments later to burst in tears.  They all knew what was yet to come.  She closed her eyes in
her mother’s caring arms while her dad hugged both of them tight.

Even from behind the tears they saw when that *something* appeared in the middle of the room.  A
thin but beautiful woman; her clothes, as if they were made of millions of ribbons, waved in the
air at each of her steps.  She was beautiful and scary as she approached the bed, covering the
vibrating light of the ceiling neon lamps.

The little girl looked up and smiled.  Then, as if she knew this tall woman long ago, she got up
from the bed and hugged her.  The woman took her hand, but before they left she said “thank you
for taking care of her.  You were very good parents, showing everyone you can raise a child even
if she’s this fragile.  You loved her and raised her as a bonding family even if you knew I will
come for her soon.  It couldn’t happen any other way.  Please, never forget this little Soul as
you gave so much to her.  Still, know that where we are headed now, she won’t have any similar
problems.  She will return anew when the time comes.  You succeeded on the first trial, but what
comes next will be much harder.  Let her go so she can take this journey.”

Then she turned away.  Spreading her huge, purple, angel-wings they left through a gate, to the
place from where there is no return.

*Based on a true story.*

Little Emma was family to me and died at the age of six, with chronic eating disorder the doctors
barely understand.  She had six heart attacks before she turned two.  Her parents were with her
until the last moment.  Unfortunately, they failed their second trial and ended up divorcing.  I
wrote this back in 2012, after her burial, knowing the purple winged Angel of Death took her to
that place she promised.  Blessed be, tiny Soul.
