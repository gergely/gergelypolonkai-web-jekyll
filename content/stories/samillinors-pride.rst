Samillinor’s Pride
##################

:date: 2012-08-10T11:15Z
:category: stories
:status: published

It was time again.  Day after day it was the same, and Samillinor was more and more upset about
it.  Since the temple was built her daily routine was the same: wake up with the Light, prepare
the ritual, make the ritual, fail the ritual, eat something which she felt like it had no taste at
all, prepare, make and fail another ritual, then go to sleep with the Light.  Today must be
different or she will go mad for sure.

She entered the temple and headed towards the altar.  She grabbed the clothing of a random
apprentice or hers just to get some attention and took him to the fire while ordering the others
to follow them.  She then tossed the apprentice back into the crowd and began speaking.

“As you all know, we are trying to get our beloved Lady Mirinar back to Erodar.  It’s a holy
mission, much bigger than survival itself.  But we continuously fail.  You, and I mean all of you,
will never see the Lady of Fire in this life.  Even I fear of this sometimes, and my lifespan is
going to be much longer than yours.  But I tell you this: if you try hard, we may at least summon
the powers of the Lady back to our hands.  I’ve met the Prophets of other Gods.  Some of them are
trying to achieve the same thing.  They fail miserably, just like us.  We have failed many times,
and many of you… *left* in the process.  I know you are all fed up with this, so I don’t blame you
if you leave now.”

She paused for a brief moment to see the outcome of this not-so-good speech.  Murmuring started in
the crowd, and soon a few Souls left the hall.  After a short while, Samillinor continued.  But
unlike the theatrical voice she used just a moment ago, she sounded more serious.

“The lava beasts will take care of those traitors.  Now listen to me.  I did not sleep last night.
Although that is not rare, its cause was something different.  I have seen something like the
Gods’ visions.  It wasn’t exactly the same, but it was strong, and it felt true.  And if it is
indeed true, we will succeed today.”

The apprentices were all prepared to hear the same speech as almost every day.  Most of them were
surprised that she allowed some of them to leave, even if that was only part of the act, but only
now they looked up to their leader.  She looked exhausted, but unlike her usual tone, this time
her voice was really cheering.  They started murmuring again, that maybe she’s right this time,
while others were afraid of some accident.

Apprentices of lesser ranks started to prepare today’s ritual.  Some others left for the personal
quarters, hardly believing Samillinor’s words.  Geoth, one of Samillinor’s oldest abbots, stepped
next to the High Priestess.

“Lady Samillinor, you look exhausted.  Are you sure you don’t want to postpone today’s ritual?”

“You know very well that simply because I’m tired, I won’t die.  I can’t die at all, actually.
Why would I postpone it?”

“Yes, I know, I know.  I’m just… you look troubled, High Priestess.  Many left for their private
quarters because they fear of a big accident today.”

“Whoever fears the powers of Lady Mirinar I will personally feed to the lava beasts.  Now leave
and make the preparations!”

“Yes, your Highness!”

Mentioned preparations were fast, as always.  Apprentices of lowest ranks swept the floor, others
changed the pelmets.  A tamed lava beast brought in a stone cauldron filled with hot lava, so the
maids could refresh the lights.  Meanwhile the fire-starters prepared the bonfires.  Finally, as
the last step, Geoth brought the ritual bowl and put it on the altar.

The timing was perfect, the ritual began with Liran on the top of the sky.  Samillinor stood
behind the altar in her ritual robe embroidered with flame patterns.  Her red hair also looked
like flames.

“Fellow apprentices of Mirinar!” Samillinor began, with the usual theatrical moves.  “We are here
once again to summon the powers of our beloved Lady back to Erodar.  So let the ritual begin!
Fire-starters, light the bonfires!”

The accosted grabbed their torches immediately, dipped them into the lava and lit the bonfires.
While they caught flame, all the apprentices started to pray.  They asked their Lady to send a
sign, and to allow them to once again use her powers on Erodar.

The bonfires lit quickly; the fire-starters were proud that they collected dry enough wood for
them.  The hot air started circulating in the well constructed building.  It blew Samillinor’s
hair so it much more resembled flames.  She dipped the bowl into the lava cauldron, and held it
high.  Then she cited the old words; her apprentices thought these are summoning words, but in
fact, they weren’t.  They were used to transform the hot lava into water; although she could have
swallowed lava without being harmed if she wanted, it gave her hard times for a few days.

The next moment it turned out that Samillinor was right: something was very different this time.
As soon as she raised her bowl, the bonfires started to burn with huge flames.  The apprentices
were surprised, but didn’t stop praying.  Samillinor closed her eyes, held the bowl at her mouth
and drank its contents.  The first swallow of lava surprised her so much that she dropped the
bowl.  The burning material inside spouted and her robe caught fire.  Her senses blurred as she
felt a strong presence.  There was no doubt, it was Mirinar, her beloved Lady.  She used her body
to materialize on Erodar once again.

Mirinar stretched and looked around.  She was standing in a small circle of ash, the remains of
Samillinor’s robe.  The altar survived.  Nice stone contraption, made of Roban-stone that endures
the hottest lava.  Between them lied a small bowl made of the same material, with cooling lava all
around.  She looked up.  Her body was still Samillinor’s, but her eyes were made of actual flames,
and so was her hair.  She looked at the apprentices around who stood shocked and in awe.

“Finally” she said, taking a step next to one of the bonfires and looked in the eye of the
fire-starter in front of her.  There stood a short lava Duaron, his skin black and eyes red like
lava.  She grabbed his wrist so strong that the Duaron dropped her torch.

“What is your name?” she asked with fury in her voice.  She knew the answer just well, she just
enjoyed speaking again.

“Harash, my Lady” said the fire-starter while he couldn’t look away from Mirinar’s eyes.

“How long do you serve the Fire?”

“Several years, my Lady.  I don’t keep track, shame on me.”

“It has been twenty-seven years and nine days, Harash.  You seem to serve me well.  So tell me… do
you want to become my first Priest?”

Harash could not speak.  That was what he wanted since he first saw the temple as a child.

“Yes, that was my only wish since I entered this temple for the first time.  But I’m still a long
way from there.”

“Indeed, Harash, you are a long way.  I don’t need a Priest who is not as committed as he can be.”

Mirinar, while still holding the wrists of Harash, looked at the other apprentices.  Harash
started feeling the anger boiling inside him.  He served Mirinar for almost three decades, and she
says he’s not worthy enough.  He couldn’t see Mirinar’s face, but she was smiling.  And soon,
Harash burst out, forgetting the fact that a God was holding his wrist”

“How dare you say that?  I am serving your purpose for decades.  I fed your beloved lava beasts, I
swept these floors for years, I can make the biggest bonfires of all the apprentices.  If I was
given a chance, I could serve you much better than anyone else in this room, and you still think
I’m not worthy enough?”

Mirinar turned back, still smiling.  Harash was confused, but was still filled with anger.  Soon,
Mirinar made a sign for Harash with her other hand to turn around.  So he did: the bonfire behind
him became so huge its flames reached the ceiling.  Until now he didn’t even feel that the air
circulating in the room became so hot that some thin textiles caught fire.  “There it is, your
real power” she whispered without anyone else than Harash hearing it.  Harash stood in awe.

“That’s the Priest I need” said Mirinar to the others, and released Harash’s wrist, and forced him
to turn back at her.  “Now look into my eyes, Harash, and tell me once again: do you want to
become my first Priest?”

The Duaron, still filled with fiery rage, stood her stare for a minute. “Yes, I do!” he finally
answered with confidence in his voice.

The bonfire collapsed under its own weight, but as it was not controlled, it collapsed right on
Harash.  He was used to even lava, but this time the heat was unbearable.  He screamed as he felt
all his bodies burning, although he was sure he’s not dying.  It felt like forever before he could
dig himself out of the embers.  He stood proud before Mirinar, who was pointing at his bare chest.

Harash looked down.  A large symbol, Mirinar’s holy sign was flaming on his skin.  Despite the
looks, it felt confortably warm.  Harash felt pride; his childhood dreams came true.  Not just he
met with his beloved Lady, but was initiated by her personally.

Mirinar turned around.  With Harash on her side she looked at the crowd.  Most of them were still
shocked by the almighty presence and what they just witnessed.  Some, especially Samillinor’s
abbots were filled with rage that a lowly fire-starter got initiated by the Lady instead of them.
Mirinar just stared at them, still smiling.

“The Parents, thank to your constant prayers, allowed us back to Erodar once again” she said and
then paused.  The apprentices started to regain their consciousness.  Finally they realised that
after all these years, all the failed rituals, today they reached their goal, and summoned their
Lady on Erodar.

“That’s the good news” Mirinar continued, before anyone could start to applaud.  “The bad news is,
you are the first and last to see me in person.  The new rules are strict.  We are able to
materialise once in front of, or through our Prophet.  After that, we can only communicate with
them through visions.  Them, and our new Priests.  Priests get all our power for their loyalty.”

The crowd became confused.  They were happy about Mirinar’s presence, but they couldn’t yet
understand these new rules she was talking about.

“Now, what should I do with you…?  Is there anyone who prayed to any of my brothers or sisters?”

Everyone stood in silence.

“Come on, don’t be shy!  In fact, if you did, you won’t survive the next few minutes.  Or if you
will, you will be sorry to do so.  So once again.  If there is anyone among you who have prayed to
any other Gods, leave now, and I mean immediately! Also leave if you think you couldn’t bear the
godly powers and don’t want to become my Priest.”

No one moved.  They were all ready, or at least thought to be ready for the task.

“Good.  See you in the Passage soon!”

Samillinor passed out.  When she woke up, her temple was gone.  All she could see was cooled lava
and ashes.  Amongst them stood her apprentices, or at least most of them.  They were talking in
excitement, in small groups among the ruins.  Goeth sat next to her on a stone, guarding her body.

“I guess that means everyone was right about this ritual” he said, while helping Samillinor to
stand up.

“So I guess it wasn’t just a vision” she replied.

“No, it was very real” said Goeth, gesturing at his chest.  He had Mirinar’s burning symbol, just
like everyone else around.

“I still don’t know if I should be proud or sad.  I was waiting to see my Lady for an Age, and
when she comes I cannot see her.  Yet, she was using my very body to materialise.  That’s not
something you feel every day.”

She stood up and looked at her body thoroughly.  Now that all her clothes were burned to ashes,
she could see that she was in a much better shape than she remembered.  Her skin looked younger
and she felt the energy burning inside.

“Whatever happened, we won.  Mirinar is back, and I have a feeling we have made it first.  Send
word to the harbour to prepare my ship.  I need to get to Turamo’r as soon as possible.”  She
paused for a moment to look around before she continued.  “Oh, and arrange the making of a huge
amount of robes.  Members of the Order of Fire cannot walk Erodar naked.”
