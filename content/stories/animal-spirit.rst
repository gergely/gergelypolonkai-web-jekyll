Animal spirit
#############

:date: 2012-09-18T16:20Z
:category: stories
:status: published

They surprised her; four men, overpowering her for sure.  It rarely happened to her, but she was
prepared.  The men were smiling in lusciousness; they were circling around her for a while, but
this was enough for her to ask help from the Spirit world.  She closed her eyes, humming.  They
didn’t even hear it, just saw that she’s tapping with her feet.  A scratch in the air; a scar
appears on the face of the man just in front of her.  It wasn’t deep, but it was obvious it
couldn’t be done by the girl, standing about five feet from them.  They looked at each other and
sprang at the girl as one, but she evaded them with a cat’s dexterity.  She got behind them
easily.  Her humming could be heard well now, although they couldn’t understand her words.  Her
feet were still doing the rhythmic tapping.

The attackers got nervous and furious, and this was even strengthened by the scratches they got
seemingly from nothing.  They got tired and pain took over their minds.  They attacked with anger
and they mostly followed the girl’s voice.  Through the mist of anger and pain, in the corner of
their eyes they saw what is happening, but it was too late.  The black panther followed the moves
of her protégé, disarming all four men.
