Coincidents
###########

:date: 2015-08-04T17:21Z
:category: stories
:status: published

They were both without someone for a while now without any intention, spoken or not, to find a new
one.  They sat next to each other on the train and listened to music.  They both peeked to each
other’s screen to check what the other one is listening to and it turned out to be the same album
of the same artist.  “Strange coincident” they both thought and looked away.  The same thing
happening three times on consecutive days, though, couldn’t have been a coincident.

On the fifth day one of them left the earphones at home; it was malfunctioning thus useless.  They
sat next to each other again, more of chance than by will.  The other one saw the lack of
earphones and offered half of theirs without words.  They listened to the same music again.  The
next day this other one brought a splitter and a second pair of earphones to make things better.

The other day, one of them brought an e-book reader and started reading a book just when the other
sat down.  Just out of curiousity the other peeked into the book, started reading just to find out
if they have the common grounds in books, too.  They waited for each other at the end of each page
without signals.  This continued until they reached the end of the book; by then, they almost
cuddled each day.

Finally, one of them decided to speak.  “We totally forgot to introduce ourselves, my name is…”
the sentence started, but couldn’t be finished; it was interrupted by a passionate kiss.
