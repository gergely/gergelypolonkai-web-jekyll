A new home
##########

:date: 2016-05-09T15:19Z
:category: stories
:status: published

This is an excerpt from a novel of mine.  The tree is the prophet of the highest god in that
universe and thus can see everything that was or will ever be.  It is said that whoever sleeps
among its roots or branches will see colourful dreams about these stories, so it is often visited
by bards.

Zuuron and Niminer couldn’t go any farther.  It looked like there is no way out of this grove, as
the way they came in was blocked by the angry mass who wanted to catch them.  A breeze came
suddenly, and it sounded as the leaves were talking.  “Come, rest between my roots.  I’ll hide you
until they get away.”

Having no other choice, the couple hid among the roots and waited.  The tree kept its promise and
hid them well, and when the mob left the grove they came out.  While they were fleeing, they
didn’t recognize how beautiful this place was: green grass was everywhere, and colorful fruits
were hanging from the nearby trees like little lanterns.  They fought themselves, as they didn’t
want to leave.

“You don’t have to” the tree whispered, as if it can read their minds.  ”They won’t find their way
back here, not until I let them.  You can live here in peace.”

In the following years they have built a home around the tree while listening the stories it told
about times even their grandparents could not remember.  And as the tree went on with these
stories even after they finished building, they started painting and writing them down, and they
hung their creations on the tree’s branches so others who may come after them can remember, too.
