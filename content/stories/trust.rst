Trust
#####

:date: 2015-02-13T08:10Z
:category: stories
:status: published

The girl was small both in height and stature.  Her blonde hair was mostly covered by a cute hat.
She stared outside the bus window with dreamy blue-grey eyes.

The guy, also thin, was so tall that he hit his head in the holding bars, usually out of reach for
anyone else, almost every time he got on the bus.  His long brown hair was stuffed under his thick
black jacket.  His ears were plugged with a pair of earphones as almost every day; he barely heard
anything, but his perception wasn’t blocked at all.  He was reading a book, but the story wasn’t
fascinating enough.  He instead used to look up at the mirror image of the girl in the window.
Sometimes they exchanged a look, but it was as short as if it was just by accident.

This continued for many days and weeks before the man gathered all his bravery and greeted her one
morning.  He didn’t insist on starting a conversation, which was obvious from his face.  It was
like a greeting of a hardly known neighbour except for the wide smile.  The girl wasn’t surprised
at all, she even returned both the smile and the greeting.  It was a big step forward for both of
them, even if they somewhat stopped at this point.

Not less than a month later came a day full of coincidences.  The man changed to a lighter coat
and left his earphones in the pockets of the old one.  He held a new book in his hand, looking for
a seat, and the only free one was next to the girl.  She looked sad, almost crying.  He greeted
her with the usual smile, which she forcefully returned.  The guy was wondering what the problem
might have been, but didn’t intend to break their unspoken customs.

“May I snuggle to you?” she asked, mostly from herself.  She spoke very low, almost whispering.
But despite the book, the man was totally aware of her and all of her actions.  Honestly, he was
preparing to ask what the matter was.

“Sure”, the reply came and he lifted his arm to hug her.  She was surprised, not because he
accepted, but that in an instant the hesitating man became so confident.  She buried her face in
the thin black coat, shivering for a moment of the cold surface.  She wasn’t really crying just
sobbing every now and then.  The man didn’t say anything, only gently slicked her hair.

They left the bus at the terminal speechless and, as usual, they went in two different ways.
Weeks passed without them meeting again.

One day the girl stood at the terminal when the man took off.  He greeted her smiling, and darted
off towards the little bakery where he bought his breakfast every day.  The girl followed him,
asking “Do you mind if I accompany you for a short while?”

“Certainly not,” he said, “I’m just buying some breakfast before taking the underground.  You want
something?”

The girl wondered.  She usually didn’t eat anything until noon, except a small bowl of cereals
every other day which she omitted today, but the offer made her hungry. “Surprise me!  I like
sweeter things,” she finally replied as they entered the shop.  The guy bought two pieces of the
same croissant, filled with chocolate, and handed one of them over.  “I hope you will like it,
these are my favourite.”

They headed to the underground station, gnawing on the croissants silently.

“Why did you do it last time?  The cuddling, I mean” she started after finishing her part.  “We do
not know each other, I don’t even know why do we greet each other in the mornings.  Hell, I don’t
even know why I dared to ask you.”  The man smiled politely without a word so the girl continued.
“I don’t even know why I am doing this conversation with a man who can say nothing, not even a
reply!”

She was becoming grumpy, and the man felt it just too well.

“Come on, you are not mute, I heard your voice several times already, and we speak the same
language!” she said with growing anger in her voice.

They just arrived to the bottom of the stairs.  In a blink of an eye, the man jumped in front of
her and hugged her.

“What do you feel?” he asked.  The girl was surprised and hesitating.  She didn’t really want to
free herself, but she was embarassed because of the last few moments.  Finally, she hugged back
and buried her face in his chest.  The coat was open, so this time she didn’t shiver, touching the
warm sweater.  She felt all too comfortable and safe, as if there were no crowd around them.  She
turned her head, sticking her ear where her face was just a moment ago.  The beating of his heart
was slow and comforting that she didn’t even want to leave that warmth.  “What do you feel?” he
asked again after a few seconds, with patience and calmness in his voice.

“Safety, I guess.” she replied, but didn’t loosen her grip.  “I feel safe besides you, even though
I do not know you or if you can be trusted.  Maybe you will drag me in a dark alley to rape and
kill me, but while I’m here close to you, I just don’t care.”

“Don’t give me ideas.“ the man grinned, though the girl couldn’t see it.  “It takes time to
achieve this deep trust.  It’s very hard to get, very easy to lose, and even harder to get back if
you do.  I’m glad I earned yours.”
