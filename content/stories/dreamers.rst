Dreamers
########

:date: 2011-02-11T11:23Z
:category: stories
:status: published

We are the Dreamers.  Others hardly believe that anyone could do it, although practically it's
possible.  You just have to believe, but most people miss this ability.

We are all different, and it’s just right this way.  Should we be similar, the world would turn
completely grey.  Of course, some think it already did; they are the ones who can’t look behind
things.  They only see monotonity because they can’t change their own, monotonous life.  But it
takes only one step to become a Dreamer, they just think they would step in a chasm.

We, Dreamers are all different, either.  Some of us just see, forward or backward in time.  Even
this is a great experience, to see what is missing from history books, or what is going to get to
them later.  But most of us can do much more.  With our will and dreams we slowly transform the
world around us to make our life more beautiful and colourful.  We can see all shades of grey, but
what is more important: we can see the colours.  We make our life beautiful with them, and the
life of people around us, too.  Because that’s our job.  Some think this is selfish because if we
can do such a thing, we must make all the bad things disappear from this world.  They are just
jealous.  They want to become Dreamers, but their lust for power makes them unable to.  They
really would be selfish.  Maybe they will learn that sadness and bad things are one of all the
colours, and besides laughing they will learn to cry a bit, too.  Because you must know that, too.
But they all teach their kids whoever cries is sad.  They just tend to forget mentioning that if
everyone would be happy, the world wouldn’t be such a funny place.  It would even become boring.

Dreamers…  As i said, we are many.  It’s easy to find us because even we find each other easily
through our dreams and intuitions.  And where we gather, huge colourful spots are created that are
visible to even the average people.  At first they will find us weird, light-headed, or ever sick.
Then they realise that the colourful world is actually a good thing and soon, one step at a time,
they become Dreamers, too.  The colourful spots become bigger and bigger this way, and who knows?
We may colour everyone a bit sooner or later.  But we are here even until then.  Us, Dreamers.
