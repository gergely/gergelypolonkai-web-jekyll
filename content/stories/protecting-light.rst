Protecting light
################

:date: 2011-12-29T13:17Z
:category: stories
:status: published

“Can you see that red mist?”

“Yes I can.  It looks like it’s closing in.” Gardan answered.

“It does.  When it gets here, close your eyes and seal yourself.”

The mist slowly engulfed them.  The girl closed her eyes and channeled all her energies to seal
herself from the outer world.

“Stronger!” Molden shouted.

“But it hurts!” she replied, almost crying.

“No, it’s the mist that burns you.  Seal!  Stronger!”

She focused, but the mist was biting her everywhere as if a desert storm was blowing tiny grains
of sand through her body.  It felt like it was eating up things from her mind that was kind or
important for her.

“This thing… it takes away my memories!”

“SEAL!” Molden shouted once more.  “Totally!  And don’t even leave until the storm goes away.”

“I can’t!” the girl shouted, crying.

At this very moment all the memories she feared for this mist burst up from her.  Her mother’s
embrace, her old and new friends who she could always counted on whether she was happy or sad.
They will protect her.  Molden would, too, if he could, but he had to care about his own survival.
She looked at the man; he was standing still, staring at the mist.  As if he didn’t feel it, as if
the millions of shards wouldn’t bite him, wouldn’t cut him from the inside.  Even his hair was
motionless, despite the strong gale around them.  How did he do it?  Nothing could be seen on his
face.  He was just standing there like a statue.  His eyes… those green eyes full of life are now
staring at the mist callously.  Then she sensed those small dots of light.  They were swirling
around Molden and was catching and diverting the red shards of the mist.  Were they emotions,
contraptions of a weird new technology, or even magic, she couldn’t decide yet.  But she realised
it is Molden who controls them.  That’s why he didn’t heed her.  He sealed himself, totally.  He’s
watching the mist to know when he can open again.

That was when she caught a glimpse of those small spheres around herself.  They were hovering next
to her, glowing white in the mist that cut to the bones.  The storm tried to sweep them away, but
the didn’t allow it.  They were the feelings that burst out of her a moment ago.  The memories she
loved so much she didn’t want to ever loose them.  Tears were bursting in her eyes while she
whispered “Help me, please!”

The globes started moving.  Though feebly, but they started to pursue the red shards.  As Gardan
felt the cuts getting weaker, she tried to strengthen those spheres, and they became lightning
fast catching and diverting the red shards of the mist.  Gardan stood still, her eyes staring at
the mist callously.
