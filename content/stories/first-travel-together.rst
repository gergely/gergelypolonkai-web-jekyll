First travel together
#####################

:date: 2012-01-03T19:23Z
:category: stories
:status: published

In the middle of the panel jungle, on the rooftop of a house a man who called himself Megron was
talking to his apprentices when the wind began to blow.  Everyone looked him up in awe that when
he was talking about the spiritual power of the air, the wind was blowing stronger and stronger.
The man stopped.  He didn’t channel energies during his speech, he didn’t even think it would be
impressive or even funny if he would.  He looked around suspiciously, but as he didn’t sense
anything out of the ordinary, he stood up, walked to the edge of the roof and looked down to the
city.  It didn’t take much time for his sharp eyes, not even from this far to see the encounter of
those two creatures.

“Get away from the roof!” he said.  “We continue later.  Molden, help them get to a secure place.”

They understood each other without words after so much time together, so Molden just nodded and
leaped towards the narrow hole they used to get to the roof.  He knew that if Megron is this
excited there must be something big happening.  That many years of training, and he can finally
use his powers today.

All of the apprentices got through the hole slowly and got to the corridor on the tenth floor,
then took the stairs to Megron’s flat on the first.  Molden has just closed the door when the
first quakes began.  The others got excited.  They used to hear about Megron’s theories about the
end of the known world is nigh, and there will be significant events until then, but none of them
thought it may turn out true.

Molden tried to calm them, told them it will be over soon but the quakes didn’t seem to stop.
They even grew stronger.  The pictures and shelves dropped from the walls and the whole house was
shaking.  The face of one of the girls was slowly taken over by worry.

“Gardan?  Is everything all right?” Molden asked putting his hand on the girl’s shoulder.  The
girl tried to hide her feelings and just nodded.

“You are worried of your parents, aren’t you?” continued Molden.

A huge tear drop rolled down on Gardan’s face.  She was fighting her emotions so she could only
nod again.

“I know you want to go but in the current situation it is a very bad idea.  Don’t worry, we take
care of them.”

Gardan could not hold it in any more and yelled everything at Molden that she never dared to tell
before.

“How could you know?  You haven’t seen your mother for years and don’t even know your father!  You
try to look like a grand master telling that you protect us, even them at the other end of the
city, but how?  You never did such a thing, just sat in the shadow of Megron and drank his words
as if you could become someone this way.  Well, you did not, and you are not even my father to
tell me what to do or what not!”

Molden got angry for a brief moment.  He was about to yell back, to show this little girl who he
actually is.  But instead he reached for Gardan’s coat.

“Close all the windows.  If they can’t stand the earthquakes, get to the inner room, as far from
any kind of glass as possible.  Megron will be here soon and help you.  Gardan, you take your coat
and we go.”

The girl, just like the others were so surprised they couldn’t say a word.  Most of them shared
Gardan’s opinion, they thought Molden is just a weak shadow of Megron who can’t do anything
without his master.  Finally, Gardan took her coat and got through the door.

They walked fast from street to street but were soon blocked by the panicked masses.  The girl
didn’t dare to say anything just followed Molden silently who was darting forward, passing by and
through the masses, choosing the least crowded streets by instinct.

It was about fifteen minutes since they left Megron’s flat of safety, but only now had Gardan
thought she should take the lead.  “He doesn’t even know where I live” she thought and she reached
out for Molden’s shoulder when the other thought came. “…yet he goes in just the right direction.
Maybe Megron told him where is each of us from.”  “No, I just see where your heart takes you” said
the man as if he was replying to her thoughts. “Or, if you like, I read your mind.”

A light chill ran down Gardan’s spine.  She felt the weight of Molden’s words and began to feel
guilty about what she said minutes before at Megron’s flat.

They may have been half way there when the earthquakes stopped and the man came to a halt.  “Can
you see that red mist?” he asked.
