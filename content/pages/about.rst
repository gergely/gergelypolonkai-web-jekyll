About the author
################

:url: about/
:save_as: about/index.html
:status: published

Coding since 1994, creating and managing Linux-based systems since 1998, using Linux as a daily
driver since 1999, a child forever, Gergely Polonkai is a systems engineer of a software company,
and also a freelancer self- and software developer.

He is learning about different IT subjects since the late 1990s.  These include web development,
application building, systems engineering, IT security and many others.  He also dug his nose
deeply into free software, dealing with different types of Linux and its applications, while also
writing and contributing to some open source projects.

Gergely likes

- to discover new things and teach them to others
- to break things and then fix them
- his job
- to play

He can turn pancake in the air, which used to be his most endorsed skill on his former linkedIn
profile.

He considers everything a game so he can remain calm even in the tightest emergencies.

   Gergely is a fun guy to work with.  He could remain cool headed in the tightest situations when
   everyone else were running around like headless chicken.

   – Graeme Spice, a former employer

He believes in the future even if he won’t see it happening, so he helps shaping it wherever he can.

On this site he is writing posts about different stuff he faces during work (oh my, yet another IT
solutions blog), hoping they can help others with their job, or just to get along with their brand
new netbook that shipped with Linux.  There might also be occasional posts about more personal
subjects from parenting and house renovation.

.. epigraph::

   I believe one can only achieve success if they follow their own instincts and listen to, but
   not bend under others’ opinions.  If you change your course just because someone says so, you
   are following their instincts, not yours.

I have my own GeekCode
======================

Because who doesn’t:

.. include:: ../files/geekcode.txt
   :code: text

And i have a `public PGP key <{static}../gergely@polonkai.eu.asc>`_
===================================================================

Its fingerprint is ``13D6 0476 B35A FCA8 BC01 3A32 F42B BA58 B074 0C4C`` and you can copy its
contents from here:

.. include:: ../gergely@polonkai.eu.asc
   :code: text
