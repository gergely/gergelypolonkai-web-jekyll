Book an Appointment
###################

:url: appointments/
:save_as: appointments/index.html
:status: published

If you want to book a short (~30 minutes) appointment with me, visit `this URL <https://ncloud.polonkai.eu/apps/appointments/pub/wyr5qQw8yw57c3tM/form>`_.

Before booking an appointment, though, you might want to contact me first (see my contact links at the bottom of the page).  Your initial message should contain the topic you want to talk about, detailed.  If you have specific questions, you should write those, too, so i can prepare in advance.  If you have a preferred media to use, like Email, XMPP/Jabber, live chat, etc. this is the best place to tell me that, too.

Here’s an example for you

    Hey,

    I’d like to chat with you about a small open service i want to create that can calculate the amount of oxygen your house plants generate.

    My main concern is about scaling; based on initial feedback, a lot of people would use it and i’m afraid the infrastructure i devised will not be able to keep up.  Right now i have 3 Raspberry Pi 4 boxes running Linux, each running 3 instances of the service.  The 4th Pi acts as a load balancer in front of the others.

    It would be great to see a friendly face between all the coding i do recently, so if we could use some video chat, that would be awesome.

    Warm regards,
    Total S. Tranger

If you prefer using disposable accounts when contacting strangers like me, please don’t get rid of the account until i reach out; it may take a day or two.

Why should you chat with me?  Who am i?
=======================================

I’m a private person with some IT experience picked up throughout the last :exp_years:`_` years.

.. important::

   The “private person” type is really important when we talk; as i mention on my `disclaimer <{filename}disclaimer.rst>`_ page, whatever i say is my own opinion, and doesn’t necessarily reflect my employers’ at the time.

Topics
======

The main reason you might want to chat with me is some technical topics.  These include, but are not limited to:

- Web backend development;
  my main interest is in the Python programming language using the Flask or FastAPI frameworks,
  but i can give general advice regardless of languages or frameworks
- General infrastructure advice;
  i actively use and maintain environments using Kubernetes and bare metal, exclusively using Linux,
  but i can most probably help with other setups, too
- Open Source tooling;
  i often make my hands dirty with new tools, although this doesn’t mean i actively use them in production.
  Still, i can provide you with up to date information on many tools around software development

I can also help reviewing articles about the above topics.

Privacy Policy
==============

What data i store
-----------------

1. Your IP address
..................

**What is it?**

An IP address is a globally unique address identifying the device you visit from.  This address is unique to you at the moment you visit the site and may or may not change over time depending on the Cafe you visit from and your Internet or VPN service provider.

**When?**

Immediately when you visit the form.

**Where?**

In the log files on my private server, operated by DigitalOcean, LLC, located in Frankfurt, Germany (on EU grounds.)

According to their `Terms of Service <https://www.digitalocean.com/legal/terms-of-service-agreement/>`_, they never access anything stored on my server unless enforced by the law, so your data is in safe hands.

**Why?**

I never use these IP addresses for anything, unless you are sending :del:`bad vibes` malicious traffic towards my web server; in such cases i will put your address on a deny-list.

I don’t even generate statistics about visitors, as i truly don’t care about such numbers.

**For how long?**

I store such logs for around 6 months, give or take a few days.

**Who do i share it with?**

No one.

Well, unless i’m forced to do so; however in such a case, all i can give to law enforcement is a list of IP addresses who visited my site, without any names attached.  They have to get that information from the provider you used when you visited my site.

2. Your name and email address
..............................

**When?**

At the moment you submit the booking form.

**Where?**

On a server with similar parameters to the one above.  This time the information goes to a database, though, not a log file.

**Why?**

So i can contact you about the appointment and send you the corresponding calendar entry.

Storing the info and sending you the invitation is done automatically by a NextCloud instance i operate on said servers.  The email with the invitation is also sent from a similar server, operated by me, so no big email providers can get it (well, unless your own address is operated by such a provider, in which case you probably don’t care about this part).

**For how long?**

I usually clean up my calendar once or twice a year, which basically means your name and email address can reside there for up to 12 months.

**Who do i share it with?**

I never share this information with anyone.

I hope you start seeing a pattern here.

3. Some details about the appointment
.....................................

This, however, depends on the means we communicate.  Some of the servers operating the services you can contact me through reside in Amsterdam, Netherlands (still on EU grounds.)

Some of my email addresses are provided by Google through their Google Workspace (formerly known as Google Apps and Google G Suite) service (if you use the contact options below you are safe and sound, though).

**When?**

As soon as you contact me.

**Where?**

On the respective service you used to contact me.  This can get rather tricky, though; see `here <{filename}contact-options.rst>`_ for information about that.

**Why?**

So we can maintain a conversation, and so that i can look up some parts while we converse.

**For how long?**

This, again, depends on the service we use.

If we use email, you can be pretty sure i will get rid of it within a few weeks.

If we use other services, the data retention policy can change by the provider.

**Who do i share it with?**

For me specifically, i will never share it with anyone.  The provider(s) we use for the communication is a different story, though.
