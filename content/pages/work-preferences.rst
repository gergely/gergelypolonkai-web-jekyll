List of conditions and preferences, for recruiters
##################################################

:url: work-preferences/
:save_as: work-preferences/index.html
:status: published

If you are a recruiter looking to hire me, i first want to say thank you.  This means you are not
just blindly select people to contact but you’re doing research on them.  I appreciate that.

Below is a list of conditions and preferences that’s good to know before we talk.  These are the
things i won’t bend in, no matter what.

* I’m a family man, and with a wife who has a much stricter schedule it is i who transport the
  children between schools and home. This means that:

  1. i need a really flexible schedule
  2. i work remotely.  If that’s not an option for your client, stop right now; that’s something
     i won’t reconsider anytime soon.  Occasional visits to HQ is OK, though.

* i use a Fedora Linux workstation for work.  I won’t install any corporate spyware on it like
  activity or time trackers.  Corporate-specific apps for things like messaging and VPN might be
  OK but many of those are suspicious.  I’m willing to run different operating systems in
  virtual machines for testing purposes, but changing my working environment must be my choice,
  not my employer’s.
* Python is my main programming language, but i’m happy to learn new ones.  I won’t, under any
  circumstances, work with Java, PHP, and Perl, unless the job is to migrate away from those
  languages.
* if i have to do some coding on a whiteboard during the interview, solve problems that are
  already solved in widely used libraries (like implementing a linked list), or solve some brain
  teasers i’ll hang up without a warning.  That method is plain wrong and no serious interviewer
  should use it.  Not even for hiring beginners.  I have a bunch of openly available projects, or
  you can put my skills to the test in a realistic setting.  I guess the work you’re hiring for
  is not “writing nonsense programs on whiteboards”.
* i will never work for Google/Alphabet, Amazon, Apple, Oracle, Meta/Facebook, or most banks and
  financial institutions.  If you are trying to recruit me for one of these, just stop; both of
  our times are more precious than that.
* i do understand that a lot of you recruiters are bound by NDAs and such, but if you find me fit
  for a position and don’t mention the company who you’re recruiting for, the position is out of
  question.  This way i can’t do *my* part of the research, to check if *i* want to work with that
  company at all.  In this case i’ll assume the worst (see the previous point) and decline
  immediately.
* money is not everything; you definitely can’t change my mind about all the things above by
  offering more.  However, if you’re recruiting in Hungary (or *from* Hungary hoping that it will
  be cheap), be prepared that my expectations are probably higher than you might think.

Please also keep in mind that i’m happily employed at the moment, and am not actively looking for
a change.  I might be interested in your offer, but there’s a high chance that i’ll just refuse.

If all the above are OK with you, feel free to contact me.  Also, instead of writing me an email,
you might want to `book a time slot <{filename}appointments.rst>`_ instead.
