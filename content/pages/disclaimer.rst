Disclaimer
##########

:url: disclaimer/
:save_as: disclaimer/index.html
:status: published

.. image:: https://i.creativecommons.org/l/by-sa/4.0/80x15.png
   :alt: Creative Commons License
   :target: http://creativecommons.org/licenses/by-sa/4.0/

This work by `Gergely Polonkai <https://gergely.polonkai.eu/>`_ is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_.


Therefore, views expressed on this site are my personal views and do not necessarily reflect the
views of my employer.  The articles and thoughts on this site are solely my own unless otherwise
stated, and therefore, they do not represent (nor are they intended to represent) the positions,
opinions or policies of my employer or any other company or person.

If you happen to use anything in your own work, please put a backlink to the given article(s) or
the main page.

The social media icons in the contact menu are made by `Rogie King <http://rog.ie/>`_.

That thing you may call the site’s design is made using `Twitter Bootstrap <http://getbootstrap.com/>`_.

Source code snippets throughout this page are licensed under `GNU General Public License v3.0
or later <https://www.gnu.org/licenses/gpl-3.0.html>`_, unless otherwise stated.

Images, audio and video files, and other media throughout this page are licensed under `Creative
Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
<https://creativecommons.org/licenses/by-sa/4.0/>`_, unless otherwise stated.

Please note that my contact information is considered to be personal data.  You can use it for
your personal means like, you know, contacting me, but i strongly and expressly decline using this
information for commercial purposes including, but not limited to, sending me marketing material.

Also note that my primary email address listed on this page is handled by Google Workspace
(formerly known as Google Apps and Google G-Suite).  While i transition to a more privacy friendly
service, i suggest you send me emails in an encrypted form; for that, you can use my `public PGP
key <{static}../gergely@polonkai.eu.asc>`_ (check the details of this key on my `about page <{filename}about.rst>`_).
