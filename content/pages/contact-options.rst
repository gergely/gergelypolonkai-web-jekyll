Contact options
###############

:url: contact-options/
:save_as: contact-options/index.html
:status: published

All the contact options below (except PayPal and LiberaPay) are hosted by myself on EU grounds
(Frankfurt, Germany or Amsterdam, The Netherlands).

Ways you can contact me, in order of preference can be seen in the footer of every page.
