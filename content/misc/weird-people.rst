On weird kids
#############

:date: 2019-11-03T08:28Z

Are you in school?  Then most probably there's a classmate of yours who is looked down by everyone
because of different behaviour, clothing, or whatever.  Give them a smile!  Not to flirt or
anything like that, just to assure them you care about their existence.  It may mean a lot to
them.  Trust me, I was in the role of the “weird kid” before.  Same applies to workplaces, or
basically any community.

After giving up the typical “weird kid noone loves” role, I made several friends this way.  In the
process I became the “weird guy everyone wants to know”; well, everyone who is fed up with their
“normal” state.
