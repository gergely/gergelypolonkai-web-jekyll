Why many people don’t know how to lead a happy life
###################################################

:date: 2015-08-12T13:34Z
:status: draft

We read about such success stories all the time: people on top of successful companies tell us how
to be successful both in their job and in their private life.  These people are usually in their
40s or 50s and, for some strange reason, they all dropped off of school around their late teen
years.  If you ask me, there are many problems with such stories.

Education is important.  Now don’t get me wrong, I don’t say these people are under-educated, they
are indeed intelligent people.  I don’t even say you can’t be successful today if you drop off
from school.  Still, as I hear the reactions of some teens to such stories, they really consider
leaving school to start their own business.  Problem is, in this business-led world of ours,
starting your own company without being funded by one of the bigger ones is almost hopeless.  And
there is one more thing to be considered here: not everyone want to become the owner of a top
company, why should they follow these “rules”?

---

I was pretty good at school, I could be in the “middle class” even if I refused to learn extra at
home (quick fact: Hungarian schools didn’t keep their students in the classrooms after 2pm in my
time, but gave a fair amount of homework).  However, I got very lazy after high school and didn’t
finish university; to be honest, I didn’t complete half of it.  And guess what: I still don’t have
my own business, yet I lead a happy life.

After reading `this story of Richard Banson <https://medium.com/p/284adfa6f6c1/>`_ from Vikas Jha,
I started thinking.  What can people do if they want to live a happy life?  Well, you should just
listen to these advises.  If you skip the first two paragraphs, you get pretty good ideas on how
to do it, the most important being “love what you do & do what you love”.  Let’s rephrase it a
bit:

“Find out what you like, and do it with passion.”

As I see, the real problem of today’s young people is that, except a few of them, they hardly know
what they want to do with their lives.
