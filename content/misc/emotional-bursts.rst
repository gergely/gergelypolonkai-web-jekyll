Emotional bursts
################

:date: 2017-10-11
:status: published

This is a reaction of mine to a post in which a father talks about an emotional burst he had when
his daughter did something nasty.  He had hard times processing it, at the end turning his anguish
to a blog post.  Unfortunately, the post is unavailable since then (i guess he deleted/renamed his
account on Medium), but my reply worths to be seen (in my opinion).

We have talked it over and over with my wife.  Not this instance of yours of course, but the
situation you got in.  We found that what you did was more than important.

Even though you write the ego is bad, sometimes it is the necessary evil.  You will find times in
your life when the only way to solve a problem is using the ego.  The ego has feelings, while the
spiritual doesn’t (you can say that everything the spiritual feels is endless love and acceptance,
but for me it seems the Spirit doesn’t *feel* it: it *is* love and acceptance).

What you did at that moment was expressing your feelings.  Us, men, are usually easier to explode,
but it’s also easy for us to calm down.  I, explode on occasions, too, but that’s pretty rare.
Sometimes I get grumpy or even mad about something.  But when it happens, I get back to my calm
state in seconds.  My wife, on the other hand, gets angrier every moment until she becomes, with
your words, a fire breathing dragon (in Hungary, however, calling your wife a dragon is usually
considered as a terrible insult).  And then she needs about the same amount of time to calm down.

The other day, our daughter was playing with plastic clay.  She ended up asking her mother if she
can put it in the aquarium for the fishes.  Mom said no, but she was eager to try it, making my
wife going up the hill.  At the end she did put some clay in the water, exactly the same time when
my wife reached the top of her anger.  She yelled at her hard while she was fishing for the pieces
of clay.  And it took her a lot of time to get back to calm grounds, while she recited “honey, I
told you many times not to put anything in the water”.  When she did calm down, she said to our
daughter she was sorry for yelling at her, but at that time the little girl was doing something
totally different, and probably forgot about the incident.

I don’t dare saying you have to yell with your kids whenever they do something bad.  If you can
get their attention, a short talk or raising your voice just a little bit is usually enough.  But
if you don’t practice your feelings before them, they won’t learn how to do it.  And trust me, a
kid without all the social skills is more than miserable.
