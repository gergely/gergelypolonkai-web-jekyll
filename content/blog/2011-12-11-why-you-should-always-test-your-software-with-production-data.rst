Why you should always test your software with production data
#############################################################

:date: 2011-12-11T12:14:51Z
:category: blog
:tags: development,testing,ranting
:url: blog/2011/12/11/why-you-should-always-test-your-software-with-production-data.html
:save_as: blog/2011/12/11/why-you-should-always-test-your-software-with-production-data.html
:status: published
:author: Gergely Polonkai

I’m writing a software for my company in PHP, using the Symfony 2 framework.  I’ve finished all
the work, created some sample data, it loaded perfectly.  Now I put the whole thing into
production and tried to upload the production data into it.  Guess what… it didn’t load.
