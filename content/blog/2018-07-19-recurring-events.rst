Recurring events are hard
#########################

:date: 2018-07-19T13:22:00Z
:category: blog
:tags: development
:url: 2018/07/19/recurring-events/
:save_as: 2018/07/19/recurring-events/index.html
:status: published
:author: Gergely Polonkai

It was almost a month ago when I `announced
<{filename}2018-06-26-please-welcome-calendar-social.rst>`_ the development of Calendar.social.
Since then I’m over some interesting and some less interesting stuff; (web) development, after
all, is just a recurrence of patterns. Speaking of recurrence, I arrived to a really interesting
topic: recurring events.

My initial thought was like “oh, that’s easy!  Let’s insert all future occurences as a separate
``Event`` object, linking to the original one for the details.  That makes handling exceptions
easy, as I just have to update/delete that specific instance.”  Well, not really.  I mean, an
event repeating daily *forever* would fill up the database quickly, isn’t it?  That’s when I
decided to look how other projects do it.

As it turns out, my first thought is about the same as everyone else has their mind, with about
the same reasons.  Then, they usually turn down the idea just like I did.  And instead, they
implement recurrence patterns and exception patterns.

My favourite is `this article
<https://github.com/bmoeskau/Extensible/blob/master/recurrence-overview.md>`_ so far.  The author
suggests to use the recurrence patterns specced by `RFC2445
<http://www.ietf.org/rfc/rfc2445.txt>`_ (the spec for the iCalendar format).  The interesting part
in this solution is how to query recurring events: you simply store the timestamp of the last
occurence of the events (or, if the event repeats forever, the greatest timestamp your database
supports.)

Choosing the maximum date seemed to be the tricky one, but it turned out both Python and popular
SQL backends support dates up to the end of year 9999.
