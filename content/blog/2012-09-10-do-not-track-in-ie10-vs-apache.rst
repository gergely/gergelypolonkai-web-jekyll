Do-Not-Track in IE10 vs. Apache
###############################

:date: 2012-09-10T20:22:32Z
:category: blog
:tags: apache,technology
:url: blog/2012/9/10/do-not-track-in-ie10-vs-apache.html
:save_as: blog/2012/9/10/do-not-track-in-ie10-vs-apache.html
:status: published
:author: Gergely Polonkai

`Apache developer decided not to accept Do-Not-Track headers from IE10 users
<http://arstechnica.com/security/2012/09/apache-webserver-updated-to-ignore-do-not-track-settings-in-ie-10/>`_,
because it’s enabled by default.  So… if I install a plugin that hides the fact from the web
server that I’m using IE10, I become eligible of using it.  But if I do this, I simply became
eligible because I consciously installed that addon, so I could actually use it without hiding the
fact.  Sorry if I’m a bit Philosoraptorish…
