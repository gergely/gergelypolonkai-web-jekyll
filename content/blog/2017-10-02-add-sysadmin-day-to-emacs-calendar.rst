Add SysAdmin day to Emacs Calendar
##################################

:date: 2017-10-02T09:37:52Z
:category: blog
:tags: emacs
:url: 2017/10/02/add-sysadmin-day-to-emacs-calendar/
:save_as: 2017/10/02/add-sysadmin-day-to-emacs-calendar/index.html
:status: published
:author: Gergely Polonkai

I’m a SysAdmin since 1998.  Maybe a bit earlier, if you count managing our home computer.  This
means `SysAdmin Day <http://sysadminday.com/>`_ is also celebrating me.  However, my Emacs
Calendar doesn’t show it for some reason.

The solution is pretty easy:

.. code-block:: lisp

   (add-to-list 'holiday-other-holidays '(holiday-float 7 5 -1 "SysAdmin Day") t)

Now invoke :kbd:`M-x holidays-list` for any year, choosing “Other” as the category, and there you
go:

.. code-block:: log

   …
   Friday, July 28, 2017: SysAdmin Day
   …
