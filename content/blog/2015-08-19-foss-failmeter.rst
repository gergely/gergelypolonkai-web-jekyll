F/OSS Fail meter
################

:date: 2015-08-19T10:12:19Z
:category: blog
:tags: development
:url: 2015/08/19/foss-failmeter/
:save_as: 2015/08/19/foss-failmeter/index.html
:status: published
:author: Gergely Polonkai

I have recently bumped into `this article <http://spot.livejournal.com/308370.html>`_.  Naturally,
I quickly calculated the FAIL metrics for all my projects (most of them are pretty high).  To ease
calculation, I made up a `small page <{static}../failmeter/index.html>`_ based on this list
(although I have divided the points by 5; I really don’t understand why spot is using such big
points if all of them can be divided by 5).  Feel free to use it, and if you have any
recommendations (point additions/removal, new categories, etc.), leave me a comment!
