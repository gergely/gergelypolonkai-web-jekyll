Changing the session cookie’s name in Symfony 2
###############################################

:date: 2012-10-13T12:49:28Z
:category: blog
:tags: symfony,development
:url: blog/2012/10/13/changing-the-session-cookie-s-name-in-symfony-2.html
:save_as: blog/2012/10/13/changing-the-session-cookie-s-name-in-symfony-2.html
:status: published
:author: Gergely Polonkai

I have a development server, on which I have several Symfony 2.x projects under the same hostname
in different directories.  Now I’m facing a funny problem which is caused by that the cookies
Symfony places for each of my projects have the same name.

To change this, you will have to modify the ``config.yml`` file like this:

.. code-block:: yaml

   session:
       name: SiteSpecificSessionName
       lifetime: 3600
