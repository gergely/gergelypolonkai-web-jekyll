Relying on my ears to keep my posture
#####################################

:date: 2020-03-25T05:48Z
:category: blog
:tags: physical-health
:url: blog/relying-on-my-ears-to-keep-my-posture/
:save_as: blog/relying-on-my-ears-to-keep-my-posture/index.html
:status: published
:author: Gergely Polonkai

**TL;DR**: my ears helped me create an almost perfect pomodoro timer out of my own body.

There’s an ongoing pandemic for a few weeks now, so whoever can should and does stay at home.
Luckily we can do it at Benchmark.games, as we only used our office to stay in physical contact
with each other because we are (somewhat) social creatures.  But now we try to take care of each
other and even the bravest work from home now.  I hope the last one who left the office turned off
the heating and the lights.

I’m also lucky because i have both a supporting family and a separate room in our house which we
didn’t really use until today.  It’s so separate that its door opens to our front yard, not inside
the house.  There is/was a lot of junk there, waiting to be sorted or to be thrown out, so i took
a day off and tidied it up a bit so i could put a table and a chair inside.  I also did some
cabling work and now it has Internet connection, too.  It’s not overly comfy, but it will do for
the few weeks/months this pandemic will (hopefully) take.  I also found a lot of treasures i won’t
list here, except a set of 5.1 speakers.

After setting up my workstation i put the speakers around me.  They are not ideally far from me
(the room is not big enough for that) but it will suffice.  The front centre speaker is above my
head so the speakers and my head form an almost perfect tetrahedron.  The back speakers are also
at the same distance behind me.  I have a bad habit of leaning closer to the screen when i overly
focus on something, and with this setup i just realised that whenever i do it i get outside of the
centre zone of the speakers and music doesn’t *sound* right.  This way i instantly stop leaning
closer to the screen and fix my posture.  Also, since the front centre speaker is above me, if my
back starts to get tired and i start to stoop, my head gets lower, and once again the music
doesn’t sound right.  I try to take mental notes whenever this happens and i realised that after
an hour it happens more often, signalling that i should take a break.

During my breaks i drink some water, visit the toilet, do some exercises, have launch, or whatever
fits me, but most importantly, i try not to think of my current work task (unless i was in the
middle of focused work, which is rare after around an hour).  This helps me clear my mind so in
the next hour i can focus better on my next task.
