SmsGateway and SmsSender
########################

:date: 2012-10-07T00:10:26Z
:category: blog
:tags: development,php,symfony
:url: blog/2012/10/7/smsgateway-and-smssender.html
:save_as: blog/2012/10/7/smsgateway-and-smssender.html
:status: published
:author: Gergely Polonkai

I have just uploaded my SmsGateway, SmsSender and SmsSenderBundle packages to `GitHub
<http://github.com/gergelypolonkai>`_ and `Packagist <http://packagist.org>`_.  I hope some of you
will find it useful.

* SmsGateway

  * `SmsGateway on GitHub`_
  * `SmsGateway on Packagist`_

* SmsSender

  * `SmsSender on GitHub`_
  * `SmsSender on Packagist`_

* SmsSenderBundle

  * `SmsSenderBundle on GitHub`_
  * `SmsSenderBundle on Packagist`_

.. _SmsGateway on GitHub: https://github.com/gergelypolonkai/smsgateway
.. _SmsGateway on Packagist: https://packagist.org/packages/gergelypolonkai/smsgateway
.. _SmsSender on GitHub: https://github.com/gergelypolonkai/smssender
.. _SmsSender on Packagist: https://packagist.org/packages/gergelypolonkai/smssender
.. _SmsSenderBundle on GitHub: https://github.com/gergelypolonkai/smssender-bundle
.. _SmsSenderBundle on Packagist: https://packagist.org/packages/gergelypolonkai/smssender-bundle
