How I started with Emacs
########################

:date: 2016-11-03T09:58:41Z
:category: blog
:tags: emacs
:url: 2016/11/03/how-i-started-with-emacs/
:save_as: 2016/11/03/how-i-started-with-emacs/index.html
:status: published
:author: Gergely Polonkai

Sacha Chua has a nice `Emacs chat intro <http://sachachua.com/blog/2013/04/emacs-chat-intro/>`_
article back from 2013.  I write this post half because she asks there about my (OK, anyone’s)
first Emacs moments, and half because I plan to do it for months now.

I wanted to start using Emacs 6(ish) years ago, and I was like “:kbd:`C-x` what”?  (Note that back
around 1998, I was among the people who exited ``vi`` by killing it from another terminal after a
bunch of tries & fails like `these <http://osxdaily.com/2014/06/12/how-to-quit-vim/>`_.)

I tried to come back to Emacs a lot of times.  And I mean a *lot*, about every two months.  I
suddenly learned what these cryptic key chord descriptions mean (``C`` is for :kbd:`Control` and
``M`` is for :kbd:`Meta`, which is actually :kbd:`Alt`), but somehow it didn’t *click*.  I
remained a ViM power user with a huge pile of 3:sup:`rd` party plugins.  Then `I found Nyan-macs
<{filename}2014-09-17-nyanmacs.rst>`_), which converted me to Emacs, and it is final now.  Many of
my friends thought I’m just kidding this being the cause, but I’m not.  I’m a huge fan of Nyan cat
(did you know there is even a site called `nyan.cat <http://nyan.cat/>`_?) and since then I have
it in my mode line:

.. image:: {static}../images/nyan-modeline.png
   :alt: Nyan modeline

…in my ``eshell`` prompt:

.. image:: {static}../images/nyan-eshell.png
   :alt: eshell prompt with a Nyan cat

…and I also `zone out <https://www.emacswiki.org/emacs/ZoneMode>`_ with Nyan cat:

.. image:: {static}../images/nyan-zone.png
   :alt: a text-based animation with Nyan cat

Now on to more serious stuff.  After browsing through all the packages provided by `ELPA
<http://elpa.gnu.org/>`_, I found tons of useful (and sometimes, less useful) packages, like `Helm
<https://github.com/emacs-helm/helm/wiki>`_, `company <http://company-mode.github.io/>`_, `gtags
<https://www.emacswiki.org/emacs/GnuGlobal>`_ (which introduced me to GNU Global, removing
Exuberant ctags from my life), `magit <https://magit.vc/>`_, `Projectile
<http://batsov.com/projectile/>`_, and `Org <http://orgmode.org/>`_ (OK, it’s actually part of
Emacs for a while, but still).  I still use these few, but in a month or two, I started to
`version control <https://github.com/gergelypolonkai/my-emacs-d>`_ my ``.emacs.d`` directory, so I
can easily transfer it between my home and work machine (and for a few weeks now, even to my
phone: I’m using Termux on Android).  Then, over these two years I wrote some packages like
`GobGen <https://github.com/gergelypolonkai/gobgen.el>`_, and a small addon for Calendar providing
`Hungarian holidays <https://github.com/gergelypolonkai/hungarian-holidays>`_, and I found a lot
more (in no particular order):

* `git-gutter <https://github.com/syohex/emacs-git-gutter>`_
* `multiple-cursors <https://github.com/magnars/multiple-cursors.el>`_
* `origami <https://github.com/gregsexton/origami.el>`_
* `ace-window <https://github.com/abo-abo/ace-window>`_
* `avy <https://github.com/abo-abo/avy>`_
* `beacon <https://github.com/Malabarba/beacon>`_

…and a lot more.

What is more important (to me) is that I started using the `use-package
<https://github.com/jwiegley/use-package>`_ package, which can automatically download packages
that are not installed on my current local system. Together with `auto-package-update
<https://github.com/rranelli/auto-package-update.el>`_, it is *very* practical.

In addition, I started to follow the blogs of a bunch of Emacs users/gurus.  I’ve already
mentioned `Sacha Chua <http://sachachua.com/>`_.  She’s a charming, cheerful person, writing a lot
about Emacs and project management (among other things).  Another one is `Bozhidar Batsov
<http://batsov.com/>`_, who, among other things, had an initiate to lay down the foundation of a
`common Elisp coding style <https://github.com/bbatsov/emacs-lisp-style-guide>`_. Another
favourite of mine is `Endless Parentheses <http://endlessparentheses.com/>`_, whence I got a lot
of ideas.
