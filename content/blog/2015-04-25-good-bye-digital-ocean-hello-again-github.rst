Good bye, Digital Ocean! Hello again, GitHub!
#############################################

:date: 2015-04-25T21:18:56Z
:category: blog
:url: blog/2015/4/25/good-bye-digital-ocean-hello-again-github.html
:save_as: blog/2015/4/25/good-bye-digital-ocean-hello-again-github.html
:status: published
:author: Gergely Polonkai

Few years ago I have signed up for a `Digital Ocean <https://www.digitalocean.com/>`_ account.  I
used one single droplet for my private needs, like hosting my private Git repositories and my
blog.  However, as I didn’t host anything else there except my blog, I decided to shut it down.
From now on, my blog is on `GitHub Pages <https://pages.github.com/>`_, as it provides just
everything I need (except automatically converting my resume to PDF.  But I can live without
that.)

I’m really sorry, Digital Ocean Guys, your hosting is awesome and I’ll keep recommending you to
others, but paying for a droplet for one single blog is overkill.
