Get account data programatically from id-manager
################################################

:date: 2016-11-18T12:43:13Z
:category: blog
:tags: emacs
:url: 2016/11/18/get-passwords-from-id-manager/
:save_as: 2016/11/18/get-passwords-from-id-manager/index.html
:status: published
:author: Gergely Polonkai

I recently started using `id-manager <https://github.com/kiwanami/emacs-id-manager>`_.  It is a
nice little package that can store your passwords, encrypting them with GPG.  My original reason
was to store my GitHub access token for `github-notifier
<https://github.com/xuchunyang/github-notifier.el>`_, but it soon turned out, it’s not *that*
easy.

``id-manager`` is a nice package when it comes to storing your password and retrieving them for
your own eyes.  But it cannot retrieve account data programatically.  Taking a look into its
source code, I came up with this solution:

.. code-block:: lisp

   (defun gpolonkai/idm-record-get-field (record field)
     "Get FIELD of an id-manager RECORD."
     (let ((funcname (intern (concat "idm-record-" (symbol-name field)))))
       (when (fboundp funcname)
         (funcall funcname record))))

   (defun gpolonkai/idm-get-field-for-account (account field)
     "Get id-manager password for ACCOUNT."
     (let ((db (idm-load-db))
           (lookup-record nil))
       (dolist (record (funcall db 'get-all-records) password)
         (when (string= account (idm-record-name record))
           (setq lookup-record (gpolonkai/idm-record-get-field record field))))
       lookup-record))

   (defmacro gpolonkai/idm-get-password-for-account (account)
     `(gpolonkai/idm-get-field-for-account ,account 'password))

   (defmacro gpolonkai/idm-get-id-for-account (account)
     `(gpolonkai/idm-get-field-for-account ,account 'account-id))

I currently need only the account ID (ie. the username) and the password, but it’s pretty easy to
add a macro to get the ``memo`` or ``update-time`` fields, too.
