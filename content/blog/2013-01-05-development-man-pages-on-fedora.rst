Development man pages on Fedora
###############################

:date: 2013-01-05T18:20:41Z
:category: blog
:tags: development,fedora
:url: blog/2013/1/5/development-man-pages-on-fedora.html
:save_as: blog/2013/1/5/development-man-pages-on-fedora.html
:status: published
:author: Gergely Polonkai

If you use Fedora (like me), and can’t find the development manual pages for e.g. ``printf(3)``
(like me), just ``yum install man-pages`` (like me).
