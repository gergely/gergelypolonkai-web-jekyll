Proxy only non-existing files with mod_proxy and mod_rewrite
############################################################

:date: 2011-06-10T14:20:43Z
:category: blog
:tags: apache
:url: blog/2011/6/10/proxy-only-non-existing-files-with-mod-proxy-and-mod-rewrite.html
:save_as: blog/2011/6/10/proxy-only-non-existing-files-with-mod-proxy-and-mod-rewrite.html
:status: published
:author: Gergely Polonkai

Today I got an interesting task.  I had to upload some pdf documents to a site.  The domain is
ours, but we don’t have access to the application server that is hosting the page yet.  Until we
get it in our hands, I did a trick.

I enabled `mod_rewrite`, `mod_proxy` and `mod_proxy_http`, then added the following lines to my
apache config:

.. code-block:: apache

   RewriteEngine on
   RewriteRule ^/$ http://172.16.72.131:8080/ [QSA,L,P]
   RewriteCond %{REQUEST_FILENAME} !-f
   RewriteRule ^/(.*) http://172.16.72.131:8080/$1 [QSA,L,P]

   Order allow,deny
   Allow from all

I’m not totally sure it’s actually secure, but it works for now.
