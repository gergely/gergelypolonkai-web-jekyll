Slugify in Python 3
###################

:date: 2016-12-08T12:54:19Z
:category: blog
:tags: development,python
:url: 2016/12/08/slugify-in-python3/
:save_as: 2016/12/08/slugify-in-python3/index.html
:status: published
:author: Gergely Polonkai

Today I needed a function to create a slug (an ASCII-only representation of a string).  I went
Googling a bit, and found an excellend `Flask snippet <http://flask.pocoo.org/snippets/5/>`_.
Problem is, it is designed for Python 2, so I came up with a Python 3 version.

.. code-block:: python

   import re
   from unicodedata import normalize

   _punctuation_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

   def slugify(text, delim='-'):
       """
       Generate an ASCII-only slug.
       """

       result = []
       for word in _punctuation_re.split(text.lower()):
           word = normalize('NFKD', word) \
                  .encode('ascii', 'ignore') \
                  .decode('utf-8')

           if word:
               result.append(word)

       return delim.join(result)

As I don’t really like the transliteration done in the first example (e.g. converting ü to ue), I
went with the second example.
