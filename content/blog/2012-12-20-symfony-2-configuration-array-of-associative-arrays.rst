Symfony 2 Configuration – Array of associative arrays
#####################################################

:date: 2012-12-20T12:03:23Z
:category: blog
:tags: php,symfony
:url: blog/2012/12/20/symfony-2-configuration-array-of-associative-arrays.html
:save_as: blog/2012/12/20/symfony-2-configuration-array-of-associative-arrays.html
:status: published
:author: Gergely Polonkai

Few days ago I have struggled with a problem using Symfony2 configuration.  I
wanted to add the following kind of configuration to ``config.yml``:

.. code-block:: yaml

   acme_demo:
       transitions:
           - { hc_cba: 180 }
           - { cba_hc: -1 }

The problem was that the stuff under ``transitions`` is dynamic, so those ``hc_cba`` and
``cba_hc`` tags can be pretty much anything.  After hitting many errors, I came to the solution:

.. code-block:: php

   <?php
   $rootNode
       ->children()
           ->arrayNode('state_machine')
               ->requiresAtLeastOneElement()
               ->beforeNormalization()
                   ->ifArray()
                       ->then(function($values) {
                           $ret = array();
                           foreach ($values as $value) {
                               foreach ($value as $transition => $time) {
                                   $ret[] = array('transition' => $transition, 'time' => e);
                               }
                           }
                           return $ret;
                       })
                   ->end()
                   ->prototype('array')
                   ->children()
                       ->scalarNode('transition')->end()
                       ->scalarNode('time')->end()
                   ->end()
               ->end()
           ->end()
       ->end()
   ;
