Please welcome Calendar.social
##############################

:date: 2018-06-26T05:36:00Z
:category: blog
:tags: development
:url: 2018/06/26/please-welcome-calendar-social/
:save_as: 2018/06/26/please-welcome-calendar-social/index.html
:status: published
:author: Gergely Polonkai

I started looking at decentralised/federated tools some years ago, but other than Matrix I didn’t
use any of them until recently.  Then this February I joined the Fediverse (federated universe) by
spinning up my own `Mastodon <https://joinmastodon.org/>`_ instance.  I’m not going to lie, this
place is pure awesome.  I follow only 53 people but unlike on Twitter or Facebook, I can’t just
scroll through my timeline; I have to read them all.  These 53 accounts are real persons over the
Internet with meaningful posts.  I could never find this on the noisy Twitter or Facebook timeline
during the last 10 or so years.

Bragging aside, and given my strive for learning, I wanted to give back something to this
community.  I thought about an image sharing platform where people can share their photo albums
with each other, but I realised I’m not that good at image handling.  So I decided to make a
calendar instead.

My app, creatively codenamed Calendar.social, aims to be a calendar service similar to Google
Calendar (and, obviously, any calendar app you can find out there) but instead of using emails, it
will use ActivityPub to share all the details (although I might add e-mail support sooner or
later.)

I have a *lot* on my mind about what this tool should/could do when it’s done.  In no particular
order, here’s a list of them:

* events that can be private (only you and the (optional) guests see them), or public (anyone can
  see them).  They will have all the usual fields like start/end time, location, and maybe an icon
  and a cover photo
* multiple calendars you would expect from a calendar app.  This way you can separate your
  birthday reminders from the dentist appointments
* event sharing over ActivityPub and other channels (to be decided, but I think you can take email
  and maybe Matrix for granted.)
* full calendar sharing.  The other party can get access from a very basic free/busy level to full
  write access (which might be a good idea for family or company wide calendars.)
* Holiday calendars that store national/bank holidays. Users can subscribe to them to see the
  holidays of a given country/area, and optionally set them as busy (on holiday weekdays) or free
  (on weekends that are actually workdays for some reason.)
* Reminders!  Because you obviously don’t want to forget the birthday of your significant other,
  your anniversary, or your barber appointment.
* All this developed with time zones, localisation, and accessibility in mind.

That, and anything more that comes to my mind.

You can follow the development `here <https://gitea.polonkai.eu/gergely/calendar-social>`_.  Also,
feel free to ping me with your ideas on my `Mastodon account <https://social.polonkai.eu/@gergely>`_, `Matrix
<https://matrix.to/#/@gergely:polonkai.eu>`_, or any other channels you can find under the
“Contact me” menu.
