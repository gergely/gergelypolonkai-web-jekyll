Moving to Pelican
#################

:date: 2019-11-05T07:05Z
:status: published

I spent the last few days with moving this site from Jekyll to Pelican.

Story time!
===========

It wasn’t just a plain conversion, though.  I also added a lot of my old stories, previously
published on Medium (i will delete that account within a few days, so it doesn’t worth linking.)
These stories are published with their original date (i wrote most of theme long before Medium
existed).

Why?
====

I left GitHub Pages for about a year or so now to host my site for myself.  (Remember when i `said
goodbye to DigitalOcean
<{filename}../blog/2015-04-25-good-bye-digital-ocean-hello-again-github.rst>`_?  Well, it seems
this goodbye was not forever…)  My reasons were mainly about privacy, and the urge to do it
myself; i probably won’t overcome this latter one, like, ever.  I want to make my hands dirty with
stuff like this.  I also believe in the web as the web, not as a centralised… thing.

Another reason was that i want to host this site not only on HTTP, but also on `DAT
<dat://gergely.polonkai.eu>`_ (there’s also a `hash link <dat://f261>`_) and IPFS.  It’s not impossible with
Jekyll, it’s just easier for me because of Python.

And thus, we arrive to my final reason.  Pelican may not have as many users as Jekyll does, but
it’s written in Python.  It’s a big plus for me as i work with Python every day.  It’s easy for me
to hack on the engine itself if i have to (and i had to several times during the migration).

Where did the comments go?
==========================

My site is a static site.  It means there’s no dynamic web engine behind it (it wouldn’n actually
work on DAT/IPFS, would it?)  It is so for a long while now.  As such, i used Disqus for comments
but, again for privacy reasons, i have removed it completely.  Also, i didn’t have comments
anyway.

What happened with the looks?
=============================

I didn’t convert my previous theme to Pelican format.  I might, in the future, as i’m not
completely happy with the new layout.  I like the new fonts, though, so maybe it will be a healthy
merger of the two.  The reason for not doing it instantly is because the old site relied heavily
on external JavaScript and CSS files (like Bootstrap) which doesn’t work well with Dat and IPFS
(yes, most Dat browsers are capable of opening them from HTTP, but then what’s the point of the
whole conversion?)  Now anyone can browse my site without an internet connection, and enjoy it in
its full glory.

What is left?
=============

I still want to do a lot of CSS tweaks, maybe converting the whole CSS part to SCSS/SASS (i
suspect the built-in theme of Pelican was written in SASS, i just need to find the source).
Nothing big, just to clean up the code.

I also want to make sure my site renders well with screen readers.  I don’t have much content, but
i want it to make accessible for everyone.  So keep an eye on the site, it might change soon!
