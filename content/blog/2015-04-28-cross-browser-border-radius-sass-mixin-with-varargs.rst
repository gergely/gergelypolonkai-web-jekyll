Cross browser border-radius SASS mixin with varargs
###################################################

:date: 2015-04-27T22:59:56Z
:category: blog
:tags: css,sass
:url: blog/2015/4/28/cross-browser-border-radius-sass-mixin-with-varargs.html
:save_as: blog/2015/4/28/cross-browser-border-radius-sass-mixin-with-varargs.html
:status: published
:author: Gergely Polonkai

Few days ago I needed to create style sheets with many rounded boxes, where different corners had
to be rounded differently (think about Bootstrap’s `button groups
<http://getbootstrap.com/components/#btn-groups>`_).

CSS has this nifty shorthand to specify border width in one line, like with ``border-width: 1px
2px 3px 4px``, but it lacks the same for ``border-radius``.  So I decided to create something
similar using `Sass mixins <http://sass-lang.com/guide#topic-6>`_ with dynamic parameters.
Another nice feature you get using the ``border-width`` shorthand is that you can specify less
than four parameters, and the values will be applied on different sides of your box, so in the end
all side will have the whole ``border-width`` set.

I wanted to achieve the same for my ``border-radius`` mixin, although I
could not start specifically with the `top` side.  I decided to go with
the top right corner for the first parameter, while trying to keep a
sane repeating pattern.  Here is the result:

.. code-block:: sass

   =border-width($t, $r: $t, $b: $t, $l: $r)
     border-top-width: $t
     border-right-width: $r
     border-bottom-width: $b
     border-left-width: $l

   =border-top-right-radius($value)
     border-top-right-radius: $value
     -moz-border-top-right-radius: $value
     -webkit-border-top-right-radius: $value

   =border-top-left-radius($value)
     border-top-left-radius: $value
     -moz-border-top-left-radius: $value
     -webkit-border-top-left-radius: $value

   =border-bottom-right-radius($value)
     border-bottom-right-radius: $value
     -moz-border-bottom-right-radius: $value
     -webkit-border-bottom-right-radius: $value

   =border-bottom-left-radius($value)
     border-bottom-left-radius: $value
     -moz-border-bottom-left-radius: $value
     -webkit-border-bottom-left-radius: $value

   =border-radius($tr, $br: $tr, $bl: $br, $tl: $tr)
     +border-top-right-radius($tr)
     +border-bottom-right-radius($br)
     +border-bottom-left-radius($bl)
     +border-top-left-radius($tl)
