cut-at-ten
##########

:status: draft
:author: Gergely Polonkai
:date: 2019-11-03T07:21Z

.. code-block:: lisp

   (defun cut-at-ten ()
     (while (re-search-forward "," (save-excursion (end-of-line) (point)) t 10)
       (newline-and-indent)))
