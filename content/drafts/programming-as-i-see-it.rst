Programming, as I see it
########################

:category: blog
:status: draft
:author: Gergely Polonkai
:date: 2017-09-17T02:00Z

since my age of around 11, i write code.  i began with basic, which is, well, the most basic
language i have ever seen.  simply writing ``10 print "hello world!"`` does the job (with assembly
it would be tens of lines as i recall).  then i moved to pascal, then delphi (which is basically
the same thing).  the next step was a bit longer, as i started learning more languages after this,
like perl (for dynamic web pages), c (for desktop applications), tcl (for eggdrop programming.
yes, i might have been a weird kid), php (again, for dynamic web pages.  it was becoming
mainstream back then).

many of my classmates looked down on me, as they thought i was a geek (hell i was, but i wouldn’t
have confessed it then), and called me a nerd.  for a few months maybe i was depressed, but after
that i realised that this is the thing i want to do in my life, this is the thing i’m good at.

Most people I ask why don’t they code say “it’s too hard”.  I’ve attended some courses (both
online and offline, and I was like “Whoa!  Coding is extremely hard!  What the hell!  I will never
learn it!”, but right after the course I realised that everything is just fine, I can still write
programs, and it’s eeeeasy.  So then, what’s the problem?

After looking through many course papers, I found that most teachers do it totally wrong.  A
programming language is just that: a language.  You don’t start learning Spanish by going into a
classic literature conference in Madrid and doing a speech, but learn the basic vocabulary and
grammar.  The same goes for coding.  You learn the vocabulary (the basic commands or keywords) and
grammar (syntax).  I had several ideas how this could be taught, just didn’t have the background
to do it.

The idea of teaching programming lingers in my head for years now, and a few days ago, I’ve bumped
into `this video <https://www.youtube.com/watch?v=dU1xS07N-FA>`_. So it seems that technology
superstars like Bill Gates and Mark Zuckerberg wants to do the same.  Maybe they don’t have enough
high quality coders at hand.  Well of course, if teachers make it awfully hard to learn it!  So a
bunch of guys sat together and created `code.org <http://www.code.org/>`_ to achieve my old dream.
I like the idea.  And although I have almost no visitor on this blog of mine, allow me to give you
a few points on how I see programming.

Great learning process
======================

When you write programs, especially during the first years, you adapt a new way of thinking and
learning.  If you learn it as an adult, it can be a bit of a pain, but as a child, it’s easy as
learning how the wheels of those little cars spin).

A job
=====

Art
===

Magic
=====
