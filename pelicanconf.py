#!/usr/bin/env python

from gergelypolonkaieu_site.plugins import custom_roles

AUTHOR = 'Gergely Polonkai'
SITENAME = 'Gergely Polonkai'
SITEURL = ''

PATH = 'content'
STATIC_PATHS = [
    '.well-known',
    'failmeter',
    'favicon.ico',
    'images',
    'gergely@polonkai.eu.asc',
    'gerrit-dashboard-generator',
    'gerrit-dashboard-generator/index.html',
    'minari',
    'minari/index.html',
    'pgpkey',
    'symbolon',
]

TIMEZONE = 'Europe/Budapest'

DEFAULT_LANG = 'en'

DEFAULT_METADATA = {
    'status': 'draft',
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = '{slug}/atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_PAGES_ON_MENU = True

# Blogroll
LINKS = (
#    ('Pelican', 'http://getpelican.com/'),
#    ('Python.org', 'http://python.org/'),
#    ('Jinja2', 'https://jinja.palletsprojects.com/'),
)

HIDDEN_PAGES = (
    'pages/appointments.rst',
    'pages/work-preferences.rst',
    'pages/contact-options.rst',
)

# Social widget
SOCIAL_WIDGET_NAME = 'contacts &amp; more'
SOCIAL = (
    ('Matrix', 'https://matrix.to/#/@gergely:polonkai.eu'),
    ('XMPP', 'xmpp:gergely@polonkai.eu?message'),
    ('Email', 'mailto:website@gergely.polonkai.eu'),
    ('Gitea', 'https://gitea.polonkai.eu/gergely/'),
    ('Liberapay', 'https://liberapay.com/gergelypolonkai'),
    ('Mastodon', 'https://social.polonkai.eu/@gergely'),
    ('Pay me a coffee', 'https://paypal.me/GergelyPolonkai/330'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
USE_FOLDER_AS_CATEGORY = True

TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'

THEME = 'my_theme'

TYPOGRIFY = True

PLUGINS = [custom_roles]
