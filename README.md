# Website of Gergely Polonkai

## Publishing to the web

```
poetry run pelican content
rsync --archive --verbose --human-readable --delete output/ /data/gergely.polonkai.eu/html
restorecon -vr /data/gergely.polonkai.eu/html
```

## Publishing to IPFS

```
poetry run pelican -s ipfspublish.py -o ipfs-version content
cd ipfs-version
ipfs add -r .
ipfs name publish --key=gergely.polonkai.eu /ipfs/XXX
```
